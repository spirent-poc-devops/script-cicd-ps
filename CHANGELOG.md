# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Automation Scripts for Spirent CI/CD Pipelines Changelog

### 1.0.0 (2021-08-26)

Initial release

#### Features
* Increment build numbers
* Release of Java components to Spirent Artifactory (private Maven repository)
* Release of Python components to Spirent Artifactory packages (private Python repository)
* Release of Node.js components to Spirent Artifactory packages (private NPM repository)
* Publishing Docker images to Spirent Artifactory (private Docker repository)
* Deployment components to CI environment
* Rollback components from CI environment

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version
