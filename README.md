# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Automation Scripts for Spirent CI/CD Pipelines

The scripts complement component dockerized build and test process with the following steps:
* Increment build numbers
* Release of Java components to Spirent Artifactory (private Maven repository)
* Release of Python components to Spirent Artifactory packages (private Python repository)
* Release of Node.js components to Spirent Artifactory packages (private NPM repository)
* Publishing Docker images to Spirent Artifactory (private Docker repository)
* Deployment components to CI environment
* Rollback components from CI environment

## Get

Get the scripts source from GitHub:
```bash
git clone git@gitlab.com:spirent-poc-devops/script-cicd-ps.git
```

## Use

Checkout the scripts in CI/CD pipelines and execute a specific script for a corresponding step in the pipeline.